# CHIFFON CAKE
## INGREDIENTS
- 5 Large eggs
- 130g sugar
- 60ml vegetable oil
- 95ml milk/water/juice
- 120g cake flour
- 6g baking powder
- vanilla extract

### Step 1
mix dry ingredients (without sugar)
### Step 2
mix wet ingredients (without eggwhites)
### Step 3
beat eggs with sugar
### Step 4
mix dry ingredients with wet
### Step 5
fold in beaten eggwhites
### Step 6
bake at 170°C for 40-45 minutes
___
rest in the form, upside down on a bottle
