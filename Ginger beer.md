# GINGER BEER
## GINGER BUG
500ml water
heaped spoon of sugar
22g of ginger
___
feed same amount every 24h
may keep in fridge feed 1/week
## GINGER BEER
### for 2 bottles
2l water
200g sugar
80g grated ginger
110g ginger bug
3 lemons
## PREPARATION
 to the pot add water ginger and sugar
 bring to the boil then simmer for 5-8 minutes
 let it cool to room temp (~20-30°C)
 add lemon juice and ginger bug, mix
 pour into flip-top bottles  (leave ~5cm of headroom)
 let them sit at room temp till fizzy to liking

 ---

 best served chilled
